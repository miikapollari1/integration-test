const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
  describe("RGB to Hex conversions", () => {
    it("converts the basic colors", () => {
      const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
      expect(redHex).to.equal("ff0000"); // red hex value

      const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
      expect(blueHex).to.equal("0000ff"); // blue hex value
    });
  });

  describe("Hex to RGB conversions", () => {
    it("converts the basic colors", () => {
      const redRgb = converter.hexToRgb("FF0000");
      expect(redRgb).to.deep.equal([255, 0, 0]); // red hex value

      const blueRgb = converter.hexToRgb("0000FF");
      expect(blueRgb).to.deep.equal([0, 0, 255]); // red hex value
    });
  });
});
